package com.altran.seu_manuel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeuManuelStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeuManuelStoreApplication.class, args);
	}

}
