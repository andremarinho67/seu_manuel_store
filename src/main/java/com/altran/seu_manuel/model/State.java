package com.altran.seu_manuel.model;

public enum State {
    NEW, APPROVED, DELIVERED, CANCELED;
}